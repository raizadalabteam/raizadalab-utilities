function zscored_mat = raj_fast_zscore(input_mat)

%%% Z-scores along the columns of input_mat

num_rows = size(input_mat,1);

input_mat_means = mean(input_mat);
input_mat_stds = std(input_mat);
input_mat_means_mat = ones(num_rows,1) * input_mat_means;
input_mat_stds_mat = ones(num_rows,1) * input_mat_stds;
input_mat_mean_corrected = input_mat - input_mat_means_mat;
zscored_mat = input_mat_mean_corrected ./ input_mat_stds_mat;   

