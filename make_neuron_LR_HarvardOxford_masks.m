HarvOxf_dir = '/data3/Raj_Neuron_data/rajspch_backup/2010_analyses/Neuron_HarvardOxford_ROIs/';
cd(HarvOxf_dir);

spm_defaults;

num_HarvOxf_rois = 48;

V_HarvOxf = spm_vol([HarvOxf_dir 'HarvardOxford_neuron_space.nii']);
m_HarvOxf = spm_read_vols(V_HarvOxf);

%%% Make left and right masks
x_size = size(m_HarvOxf,1);
left_right_midpoint = floor(x_size/2);
left_mask = zeros(size(m_HarvOxf));
right_mask = zeros(size(m_HarvOxf));
for x_val = 1:x_size,
   if x_val > left_right_midpoint,
      left_mask(x_val,:,:) = 1;
   else,
      right_mask(x_val,:,:) = 1;
   end;
end;

for roi_num = 1:num_HarvOxf_rois,
   
   disp(['ROI num: ' num2str(roi_num) ]);
   this_roi = (m_HarvOxf==roi_num);
   
   %%% Get rid of straggling disconnected voxels in the wrong place
   [connected_region_labels,num_connected_regions] = spm_bwlabel(double(this_roi),18);
   [freqs,bin_centers] = hist(nonzeros(connected_region_labels),[1:300]);
   %%% There are at most two proper regions, but sometimes only one if it
   %%% crosses the midline.
   %%% We will define a cutoff point of the number of voxels
   voxel_count_cut_off = max(freqs)/2;
   real_region_labels = find( freqs > voxel_count_cut_off );
   
   this_roi_cleaned_up = ismember(connected_region_labels,real_region_labels);
   
   left_roi = this_roi_cleaned_up .* left_mask;
   right_roi = this_roi_cleaned_up .* right_mask;
      
   leftV_this_roi = V_HarvOxf;
   leftV_this_roi.fname = [ 'left_HarvardOxford_Neuron_normed_' num2str(roi_num) '.nii' ];
   spm_write_vol(leftV_this_roi,left_roi);
   %unix(['gzip ' leftV_this_roi.fname ]);
   
   rightV_this_roi = V_HarvOxf;
   rightV_this_roi.fname = [ 'right_HarvardOxford_Neuron_normed_' num2str(roi_num) '.nii' ];
   spm_write_vol(rightV_this_roi,right_roi);
   %unix(['gzip ' rightV_this_roi.fname ]);
     
end;

%%%% Make a volume containing all the ROIs
V_all = V_HarvOxf;
V_all.fname = 'all_HarvardOxford_Neuron.nii';
m_all = zeros(size(m_HarvOxf));

for roi_num = 1:num_HarvOxf_rois,
   leftV_this_roi.fname = [ 'left_HarvardOxford_Neuron_normed_' num2str(roi_num) '.nii' ];
   leftV_this_roi = spm_vol(leftV_this_roi.fname);
   m_left = spm_read_vols(leftV_this_roi);
   
   rightV_this_roi.fname = [ 'right_HarvardOxford_Neuron_normed_' num2str(roi_num) '.nii' ];
   rightV_this_roi = spm_vol(rightV_this_roi.fname);
   m_right = spm_read_vols(rightV_this_roi);

   m_all = m_all + roi_num*m_left + (num_HarvOxf_rois+roi_num)*m_right;
end;
spm_write_vol(V_all,m_all);


   