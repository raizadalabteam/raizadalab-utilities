function [h] = save_fig(h, fig_size, filename, format)
% shortcut for saving figure to image.
% 
% save_fig(h, fig_size, filename, format)

if length(fig_size) < 2
    fig_size = fig_size * [1 1];
end

if nargin < 4
    format = 'png';
end

set(h, 'Position', [100 100 fig_size(1) fig_size(2)], 'PaperPositionMode', 'auto');

[out_dir] = fileparts(filename);
if ~exist(out_dir, 'dir')
    mkdir(out_dir);
    warning('Output directory does not exist, creating:\n%s', out_dir);
end

print(h, ['-d' format], '-r300', filename);