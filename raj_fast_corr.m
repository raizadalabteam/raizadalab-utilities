function corr_mat = raj_fast_corr(matrix_1,matrix_2)

%%% If there's only one input then we correlate that matrix with itself
if nargin == 1,
   matrix_2 = matrix_1;
end;

num_rows_1 = size(matrix_1,1);
num_rows_2 = size(matrix_2,1);
if num_rows_1~=num_rows_2,
   disp('Error: input matrices must have same number of rows');
   return;
end;
n = num_rows_1;

matrix_1_zscored = raj_fast_zscore(matrix_1);   
matrix_2_zscored = raj_fast_zscore(matrix_2);   
  
corr_mat = matrix_1_zscored' * matrix_2_zscored / (n-1);   

  